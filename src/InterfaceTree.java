import java.util.Iterator;

/**
 * Created due to issues with class construction and to ensure that division of interests is kept to allow for reusability later if needed
 */
public interface InterfaceTree<E> extends Iterable<E> {
    Position<E> root();
    Position<E> parent(Position<E> p);
    Iterable<Position<E>> children(Position<E> p);
    int numChildren(Position<E> p);
    boolean isExternal(Position<E> p);
    boolean isInternal(Position<E> p);
    boolean isRoot(Position<E> p);
    int size();
    boolean isEmpty();
    Iterator<E> iterator();
    Iterable<Position<E>> positions();
}
