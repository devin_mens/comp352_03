/**
 * Test driver created using the tree provided in the theoretical section of the assignment
 */
public class TreeDriver {
    public static void main(String args[]){
        //create and populate tree object using add* methods  for String type nodes
        Tree driver = new Tree<String>();
        driver.addRoot("F");
        driver.addLeft(driver.root, "A");
        driver.addRight(driver.root, "K");
        Position<String> leftRoot = driver.root.getLeft();
        Position<String> rightRoot = driver.root.getRight();
        driver.addLeft(leftRoot, "S");
        driver.addRight(leftRoot, "Q");
        driver.addRight(rightRoot, "L");
        Position<String> Q = driver.right(leftRoot);
        Position<String> L = driver.right(rightRoot);
        driver.addLeft(Q, "Y");
        driver.addRight(Q,"P");
        driver.addRight(L, "M");
        Position<String> Y = driver.left(Q);
        Position<String> P = driver.right(Q);
        driver.addLeft(Y, "E");
        driver.addLeft(P, "R");
        driver.addRight(P, "D");
        driver.addRight(driver.left(Y), "U");

        /**
         * Begin calls to test methods and display output for user verification
         */

        //Create iterator for InOrder traversal and loop through to display contents in order
        Iterable<Position<String>> mapping = driver.inOrder();
        for(Position<String> node : mapping)
                System.out.print(node.getElement() + ",");

        System.out.println("\n" + driver.isExternal(Y));
        System.out.println(driver.isInternal(Y));
        System.out.println(driver.height(driver.root()));
        System.out.println(driver.size());
        System.out.println(driver.numChildren(L));
        System.out.println(driver.parent(driver.root()));
        System.out.println(driver.sibling(Y));
        System.out.println(driver.isEmpty());
        System.out.println(driver.isRoot(P));
        System.out.println(driver.children(driver.root()));
    }
}
