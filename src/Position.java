/**
 * Defines the interface implementing the Position ADT to be used by the Node internal class of the concrete Tree class
 */

public interface Position<E>{
    E getElement();
}
