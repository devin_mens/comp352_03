/**
 *Parent abstract class to implement basic methods defined in the Interface InterfaceTree
 * Exists due to issues with compiler fully catching abstract methods implemented in interface
 */
public abstract class AbstractTree<E> implements InterfaceTree<E> {
    @Override
    public boolean isInternal(Position<E> p){
        return (numChildren(p) > 0);
    }

    @Override
    public boolean isExternal(Position<E> p) {
        return (numChildren(p) == 0);
    }

    @Override
    public boolean isRoot(Position<E> p) {
        return (p == root());
    }

    @Override
    public boolean isEmpty() {
        return (this.size() == 0);
    }
}
