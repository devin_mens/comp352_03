/**
 * Concrete Class to implement BinaryTree ADT as determined by the assignment
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
public class Tree<E> extends AbstractTree<E> {


    /**
     * Internal class defining state and behaviour for Tree Nodes used in Tree construction
     * This is done to ensure any Linked-List functionality is hidden from end-user
     * @param <E> Type of object/type to be stored by Tree
     */
    protected static class Node<E> implements Position<E>{
        private E element;
        private Node<E> parent;
        private Node<E> left;
        private Node<E> right;

        public Node(E element, Node<E> p, Node<E> childL, Node<E> childR){
            this.element = element;
            this.parent = p;
            this.left = childL;
            this.right = childR;
        }

        public E getElement(){
            return element;
        }

        public Node<E> getParent(){
            return this.parent;
        }

        public Node<E> getLeft(){
            return this.left;
        }

        public Node<E> getRight(){
            return this.right;
        }

        public void setElement(E element){
            this.element = element;
        }

        public void setParent(Node<E> parent){
            this.parent = parent;
        }

        public void setLeft(Node<E> left){
            this.left = left;
        }

        public void setRight(Node<E> right){
            this.right = right;
        }

        public Node<E> deepCopy(){
            if(this == null){
                return null;
            }
            Node<E> clone = new Node(this.getElement(), this.getParent().deepCopy(), this.getLeft().deepCopy(), this.getRight().deepCopy());
            return clone;
        }

        public String toString(){
            return (String)this.getElement();
        }

    }

    protected Node<E> createNode(E element, Node<E> parent, Node<E> left, Node<E> right){
        return new Node<E>(element, parent, left, right);
    }

    protected Node<E> root = null;
    private int size = 0;

    /**
     * Default Constructor to create empty Tree structure
     */
    public Tree(){
    }

    public int size(){
        return this.size;
    }

    public Position<E> root(){
        return this.root;
    }

    public Position<E> parent(Position<E> p){
        Node<E> node = (Node<E>)p;
        return node.getParent();
    }

    public Position<E> left(Position<E> p){
        Node<E> node = (Node<E>)p;
        return node.getLeft();
    }

    public Position<E> right(Position<E> p){
        Node<E> node = (Node<E>)p;
        return node.getRight();
    }

    /**
     * Return number of direct children of Position p
     */
    public int numChildren(Position<E> p){
        int count = 0;
        if(left(p) != null){
            count++;
        }
        if(right(p) != null){
            count++;
        }
        return count;
    }

    /**
     * Return pointer to sibling of position p if exists
     */
    public Position<E> sibling(Position<E> p){
        Position<E> parent = parent(p);
        if(parent == null){
            return null;
        }
        if(p == left(parent)){
            return right(parent);
        }
        else
            return left(parent);
    }

    /**
     * Similar to numChildren method, but returns Iterable of children instead of simple count
     */
    public Iterable<Position<E>> children(Position<E> p){
        List<Position<E>> dependants = new ArrayList<Position<E>>(2);
        if(left(p) != null){
            dependants.add(left(p));
        }
        if(right(p) != null){
            dependants.add(right(p));
        }
        return dependants;
    }

    public int height(Position<E> p){
        int height = 0;
        for(Position<E> c: children(p))
            height = Math.max(height, 1+height(c));
        return height;
    }

    /**
     * Helper method to populate a recursive subtree with passed position as root
     */
    private void inOrderSubTree(Position<E> p, List<Position<E>> mapping){
        if(left(p) != null){
            inOrderSubTree(left(p), mapping);
        }
        mapping.add(p);
        if(right(p) != null){
            inOrderSubTree(right(p), mapping);
        }
    }

    /**
     * Inorder traversal algorithm using above helper method to populate traversal list with tree nodes
     */
    public Iterable<Position<E>> inOrder(){
        List<Position<E>> mapping = new ArrayList<Position<E>>();
        if(!isEmpty()){
            inOrderSubTree(root(), mapping);
        }
        return mapping;
    }

    public Iterable<Position<E>> positions(){
        List<Position<E>> mapping = new ArrayList<Position<E>>();
        if(!isEmpty()){
            inOrderSubTree(root(), mapping);
        }
        return mapping;
    }

    /**
     * Inner helper class to handle Iterator functionality for List representation of tree
     */
    private class Eterator implements Iterator<E>{
        Iterator<Position<E>> positionIterator = positions().iterator();
        public boolean hasNext(){
            return positionIterator.hasNext();
        }
        public E next(){
            return positionIterator.next().getElement();
        }
        public void remove(){
            positionIterator.remove();
        }
    }

    public Iterator<E> iterator(){
        return new Eterator();
    }

    public int numleaf(Node<E> node) {
        if(node == null)
            return 0;
        if(node.getLeft() ==null && node.getRight()==null)
            return 1;
        else
            return numleaf(node.getLeft())+ numleaf(node.getRight());
    }

    public Position<E> addRoot(E element){
        root = createNode(element, null, null, null);
        size = 1;
        return root;
    }

    public Position<E> addLeft(Position<E> p, E element){
        Node<E> parent = (Node<E>)p;
        Node<E> child = createNode(element, parent, null, null);
        parent.setLeft(child);
        size++;
        return child;
    }

    public Position<E> addRight(Position<E> p, E element){
        Node<E> parent = (Node<E>)p;
        Node<E> child = createNode(element, parent, null, null);
        parent.setRight(child);
        size++;
        return child;
    }

    /**
     * Merge any two given binary trees with passed position as root junction
     */
    public void merge(Position<E> p, Tree<E> leftTree, Tree<E> rightTree){
        Node<E> node = (Node<E>)p;
        if(!isInternal(p)){
            size += leftTree.size() + rightTree.size();
        }
        if(!leftTree.isEmpty()){
            leftTree.root.setParent(node);
            node.setLeft(leftTree.root);
            leftTree.root = null;
            leftTree.size = 0;
        }
        if(!rightTree.isEmpty()){
            rightTree.root.setParent(node);
            node.setRight(rightTree.root);
            rightTree.root = null;
            rightTree.size = 0;
        }
    }


}
