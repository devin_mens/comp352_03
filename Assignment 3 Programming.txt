Question 1.

Tree Psuedo Code


Clone Pseudo

Function:Clone
Input:Node current , Node Clone
Output: BinaryTree TreeClone

if( current not Null){
	return null
}
if(root.left exists){
	Clone.left = new Node
	Clone(current.left, Clone.left)
}
if(root.right exists){
	Clone.right = new Node
	Clone(current.right, Clone.right)
}


Traversal Pseudo

Function:InOrder
Input:Position Node, Array[] mapping
Output:Array[] mapping containing inorder traversal of nodes
	if( node.left exists){
		InOrder(node.left, mapping)
	}
	mapping.add(node)

	if( node.right exists){
	InOrder(node.right, mapping)
	}

Question 4.

The overall complexity typically worsens, with any modification operations to the tree requiring O(n) time, since the nature of an array tree is non-propagating in the manner that a linked-list tree would be while also requiring that all nodes exists in a non-isolated manner


Question 5.
In the case of linked-list trees, time complexity for all operations take place in O(1) time with the following exceptions,
children = O(c+1) where c is the number of children beneath the node
depth = O(d+1) where d is the level depth of the node
height = O(n) since each node must be travelled to to ensure that all children/leaves are found to determine the total height

Space Complexity for a linked-list is done in O(n), since only references and nodes are created as needed with no superfluous space usage.

Array Trees as noted above are take a general time of O(n) with the exception taken as noted above. 
Space complexity follows the trend of worsening behaviour but to a larger extent, will the requirement for space being O((2**n)-1) since each node has its children at I[2Node] or I[2Node +1], this means that in cases for non complete tree will the root node placed at I[1] and leaving an empty I[0] slot